package com.example.android.alarmmanagerdemo;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.sql.Struct;
import java.sql.Time;

public class MainActivity extends AppCompatActivity {
    ToggleButton mAlarmToggle;
    private static final int NOTIFICATION_ID = 0;
    private static final String ACTION_NOTIFY="com.example.android.alarmmanagerdemo";
    AlarmManager alarmManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        alarmManager= (AlarmManager) getSystemService(ALARM_SERVICE);

        mAlarmToggle=findViewById(R.id.alarmToggle);
        Intent notifyIntent = new Intent(ACTION_NOTIFY);
        final PendingIntent notifyPendingIntent=PendingIntent.getBroadcast(this,
                NOTIFICATION_ID,notifyIntent,PendingIntent.FLAG_UPDATE_CURRENT);

        mAlarmToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String toastMessage;

                if(isChecked){
                    toastMessage=getString(R.string.alarm_on_toast);
                    long triggerTime = SystemClock.elapsedRealtime();
                    long repeatInterval = AlarmManager.INTERVAL_FIFTEEN_MINUTES;
                    alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                            triggerTime, repeatInterval, notifyPendingIntent);
                }
                else{
                    alarmManager.cancel(notifyPendingIntent);
                    toastMessage=getString(R.string.alarm_off_toast);

                }
                Toast.makeText(MainActivity.this, toastMessage, Toast.LENGTH_SHORT).show();
            }
        });
    }

}
