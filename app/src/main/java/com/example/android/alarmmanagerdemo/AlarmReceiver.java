package com.example.android.alarmmanagerdemo;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

public class AlarmReceiver extends BroadcastReceiver {

    NotificationManager mNotificationManager;
    int NOTIFICATION_ID=0;
    @Override
    public void onReceive(Context context, Intent intent) {
        mNotificationManager= (NotificationManager) context.getSystemService
                (context.NOTIFICATION_SERVICE);
        deliverNotification(context);

    }
    private void deliverNotification(Context context){


        Intent contentIntent=new Intent(context,MainActivity.class);

        PendingIntent contentPendingIntent=PendingIntent.getActivity(context,NOTIFICATION_ID,
                contentIntent,PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder=new NotificationCompat.Builder(context,"1")
                .setSmallIcon(R.drawable.ic_standup)
                .setContentTitle(context.getString(R.string.notification_title))
                .setContentText(context.getString(R.string.notification_text))
                .setContentIntent(contentPendingIntent)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setDefaults(NotificationCompat.DEFAULT_ALL);

        mNotificationManager.notify(NOTIFICATION_ID,builder.build());

    }
}
